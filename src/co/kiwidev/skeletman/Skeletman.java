package co.kiwidev.skeletman;

import java.util.ArrayList;

import lombok.Getter;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Skeletman extends JavaPlugin {

    @Getter private static Skeletman instance;
    @Getter private ArrayList<Skeleton> skeletmans;

    @Override
    public void onEnable() {
        instance = this;
        skeletmans = new ArrayList<>();

        new SkeletmanTask().runTaskTimer(this, 20, 1);
    }

    @Override
    public void onDisable() {
        instance = null;

        for(Skeleton skeleman : skeletmans) {
            skeleman.remove();
        }

        skeletmans.clear();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!sender.isOp()) {
            sender.sendMessage("You're not OP!");
            return false;
        }

        if(!(sender instanceof Player)) {
            sender.sendMessage("Players only!");
            return false;
        }

        Player player = (Player)sender;
        Skeleton skeleton = (Skeleton)player.getWorld().spawnEntity(player.getLocation(), EntityType.SKELETON);
        skeleton.getEquipment().setChestplate(setColor(Color.RED));
        skeleton.getEquipment().setItemInHand(null);
        skeletmans.add(skeleton);
        sender.sendMessage("Spawned a skeletman");
        return true;
    }

    public ItemStack setColor(Color color) {
        ItemStack item = new ItemStack(Material.LEATHER_CHESTPLATE);
        LeatherArmorMeta meta = (LeatherArmorMeta)item.getItemMeta();
        meta.setColor(color);
        item.setItemMeta(meta);
        return item;
    }

}
