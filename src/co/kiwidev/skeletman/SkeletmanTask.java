package co.kiwidev.skeletman;

import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import co.kiwidev.utils.Sender;

public class SkeletmanTask extends BukkitRunnable {

    private int color = 0;
    private boolean increase = true;

    private final Vector BLANK_VECTOR = new Vector(0, 0, 0);
    private final Player CAKE_PLAYER = Bukkit.getPlayer("CoderCake");

    public void run() {
        Iterator<Skeleton> iter = Skeletman.getInstance().getSkeletmans().iterator();

        while(iter.hasNext()) {
            Skeleton skeletman = iter.next();

            if(!skeletman.isValid()) {
                iter.remove();
                continue;
            }

            ItemStack chestplate = skeletman.getEquipment().getChestplate();
            LeatherArmorMeta meta = (LeatherArmorMeta)chestplate.getItemMeta();
            meta.setColor(meta.getColor().setRed(color).setBlue(255 - color).setGreen(color));
            Sender.log(meta.getColor().toString());
            chestplate.setItemMeta(meta);
            skeletman.getEquipment().setChestplate(chestplate);

            Location loc = skeletman.getLocation();
            loc.setX(loc.getBlockX() + .5);
            loc.setY(loc.getY());
            loc.setZ(loc.getBlockZ() + .5);
            skeletman.teleport(loc);

            skeletman.setFireTicks(0);
            skeletman.setHealth(skeletman.getMaxHealth());
            skeletman.setVelocity(BLANK_VECTOR);
            skeletman.setTarget(CAKE_PLAYER);
        }

        if(increase) {
            color += 1;

            if(color >= 255) {
                increase = false;
            }
        } else {
            color -= 1;

            if(color <= 0) {
                increase = true;
            }
        }
    }

}
